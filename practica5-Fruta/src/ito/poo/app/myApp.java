package ito.poo.app;
import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;
@SuppressWarnings("unused")
public class myApp {
	
	static void run() {
		Fruta f1 = new Fruta("Naranja", 5.5F, 3, 5);
		System.out.println(f1);
		f1.agregarPeriodo("dos meses", 5000);
		System.out.println(f1);	
		Fruta f2 = new Fruta("platano", 4.5F, 3, 5);
		System.out.println(f2);
		System.out.println();
		f2.agregarPeriodo("marzo", 4000);
		f2.agregarPeriodo("abril y mayo", 500);
		System.out.println(f2);
		System.out.println(f2.getPeriodos());
		System.out.println(f2.devolverPeriodo(3));
		System.out.println(f2.devolverPeriodo(1));
		System.out.println(f2.costoPeriodo(f2.devolverPeriodo(3)));
		System.out.println(f2.costoPeriodo(f2.devolverPeriodo(1)));
		System.out.println(f2.gananciaEstimada(f2.devolverPeriodo(1)));
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
	}

}
